﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Digitest.Models;

namespace Digitest.Controllers
{
    public class HomeController : Controller

    {
        private ApplicationDbContext db = new ApplicationDbContext();

        public ActionResult Index()
        {
            ClientQuestions clientQuestions = new ClientQuestions();

            clientQuestions.Questions = db.Question.Include("AnswerChoice").Include("DragDropLeft").Include("DragDropRight").ToList();
            foreach (var question in clientQuestions.Questions)
            {
                if(question.AnswerTypeId == 4)
                {
                    question.DragDropRight = question.DragDropRight.OrderBy(a => Guid.NewGuid()).ToList();
                }
            }


            if (!User.IsInRole("Admin"))
            {

                return View(clientQuestions);
            }

            return RedirectToAction("Index", "AnswerChoices");
        }


        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult SaveData(FormCollection collection)
        {
            var client = new Client();
            if (ModelState.IsValid)
            {
                if(collection.Get("Client.Permission").StartsWith("true"))
                {
                    client.FirstName = collection.Get("Client.FirstName");
                    client.LastName = collection.Get("Client.LastName");
                    client.Age = Convert.ToInt32(collection.Get("Client.Age"));
                    client.Gender = (Gender)Convert.ToInt32(collection.Get("Client.Gender"));
                    client.Email = collection.Get("Client.Email");
                }
                else 
                {
                    client.Permission = false;
                    client.FirstName = "test";
                    client.LastName = "";
                    client.Age = 1;
                    client.Gender = 0;
                    client.Email = "";
                }
                db.Client.Add(client);
                db.SaveChanges();

                var questions = db.Question.Include("AnswerChoice").Include("DragDropLeft").Include("DragDropRight").Include("AnswerType").ToList();
               
                foreach (var question in questions)
                {
                    foreach (var answerChoice in question.AnswerChoice)
                    {
                        ClientAnswer clientAnswer = new ClientAnswer();
                        //clientAnswer = new ClientAnswer();
                        clientAnswer.QuestionId = question.Id;
                        clientAnswer.ClientId = client.Id;

                        if (question.AnswerType.TypeName == "CheckBox")
                        {
                            var answer = collection.Get("checkbox_" + answerChoice.Id);
                            if (answer.StartsWith("true"))
                            {
                                clientAnswer.AnswerChoiceId = answerChoice.Id;
                                db.ClientAnswer.Add(clientAnswer);
                            }
                        }
                        else if (question.AnswerType.TypeName == "RadioButton")
                        {
                            var answer = collection.Get("radiobutton_" + question.Id);
                            if (!string.IsNullOrEmpty(answer))
                            {
                                clientAnswer.AnswerChoiceId = Convert.ToInt32(answer);
                                db.ClientAnswer.Add(clientAnswer);
                            }
                            break;
                        }
                        else if (question.AnswerType.TypeName == "TextArea")
                        {
                            var answer = collection.Get("textarea_" + answerChoice.Id);
                            if (!string.IsNullOrEmpty(answer))
                            {
                                clientAnswer.AnswerText = answer;
                                clientAnswer.AnswerChoiceId = answerChoice.Id;
                                db.ClientAnswer.Add(clientAnswer);
                            }
                        }
                     
                    }
                    
                }

                var dragLeftKeys = collection.AllKeys.Where(x => x.StartsWith("dragleft_")).ToList();
                foreach (var dragLeftKey in dragLeftKeys)
                {
                    
                    var dragRightValue = collection.Get(dragLeftKey);

                    if(!string.IsNullOrEmpty(dragRightValue))
                    {
                        ClientDragDropAnswer clientDragDropAnswer = new ClientDragDropAnswer();
                        clientDragDropAnswer.ClientId = client.Id;

                        var index = dragLeftKey.IndexOf("_");
                        int dragDropLeftId = Convert.ToInt32(dragLeftKey.Substring(index + 1));



                        string[] parts = dragRightValue.Split('_');
                        int dragDropRightId = Convert.ToInt32(parts[1]);

                        clientDragDropAnswer.DragDropLeftId = dragDropLeftId;
                        clientDragDropAnswer.DragDropRightId = dragDropRightId;

                        db.ClientDragDropAnswer.Add(clientDragDropAnswer);
                    }
                
                }
                
                db.SaveChanges();


                 var clientRightAnswerCheckRadioCounts =
                 (
                    from answer in db.ClientAnswer
                    join rightAnswer in db.RightAnswer on answer.AnswerChoiceId equals rightAnswer.AnswerChoiceId
                    join question in db.Question on answer.QuestionId equals question.Id
                    where answer.ClientId == client.Id && question.AnswerTypeId < 3

                    group answer by answer.QuestionId into g
                    select new
                    {
                        QuestionId = g.Key,
                        Count = g.Count(p => p.AnswerChoiceId > 0)
                     }
                 ).ToDictionary(t => t.QuestionId, t => t.Count);


                var clientRightAnswerTextCounts =
                (
                   from answer in db.ClientAnswer
                   join answerChoice in db.AnswerChoice on answer.AnswerChoiceId equals answerChoice.Id
                   join question in db.Question on answer.QuestionId equals question.Id
                   where answer.ClientId == client.Id && question.AnswerTypeId == 3
                      && answer.AnswerText.ToLower().Contains(answerChoice.AnswerText.ToLower())

                   group answer by answer.QuestionId into g
                   select new
                   {
                       QuestionId = g.Key,
                       Count = g.Count(p => p.AnswerChoiceId > 0)
                   }
                ).ToDictionary(t => t.QuestionId, t => t.Count);


                var rightAnswerCounts=
                (
                    from rightAnswer in db.RightAnswer 
                    group rightAnswer by rightAnswer.QuestionId into g
                    select new
                    {
                        QuestionId = g.Key,
                        Count = g.Count(p => p.AnswerChoiceId > 0)
                    }
                ).ToDictionary(t => t.QuestionId, t => t.Count);

                var clientRightAnswerDragDropCounts =
                (
                   from answer in db.ClientDragDropAnswer
                   join left in db.DragDropLeft on answer.DragDropLeftId equals left.Id
                   join dragdrop in db.DragDrop on new { answer.DragDropLeftId, answer.DragDropRightId } equals new { dragdrop.DragDropLeftId, dragdrop.DragDropRightId } 
                   where answer.ClientId == client.Id
                    
                   group left by left.QuestionId into g
                   select new
                   {
                       QuestionId = g.Key,
                       Count = g.Count(p => p.Id > 0)
                   }
                ).ToDictionary(t => t.QuestionId, t => t.Count);


                var rightDragDropAnswerCounts =
                (
                    from left in db.DragDropLeft
                    group left by left.QuestionId into g
                    select new
                    {
                        QuestionId = g.Key,
                        Count = g.Count(p => p.Id > 0)
                    }
                ).ToDictionary(t => t.QuestionId, t => t.Count);


                double points = 0;
                // checkbox and radiobutton
                foreach (var answerCount in clientRightAnswerCheckRadioCounts)
                {
                    if(clientRightAnswerCheckRadioCounts[answerCount.Key] == rightAnswerCounts[answerCount.Key])
                    {
                        points++;
                    }
                    else
                    {
                        points += clientRightAnswerCheckRadioCounts[answerCount.Key] / (double) rightAnswerCounts[answerCount.Key];
                    }
                }

                // text
                foreach (var answerCount in clientRightAnswerTextCounts)
                {
                    if (clientRightAnswerTextCounts[answerCount.Key] == rightAnswerCounts[answerCount.Key])
                    {
                        points++;
                    }
                }
                
                // dragdrop
                foreach (var answerCount in clientRightAnswerDragDropCounts)
                {
                    if (clientRightAnswerDragDropCounts[answerCount.Key] == rightDragDropAnswerCounts[answerCount.Key])
                    {
                        points++;
                    }
                    else
                    {
                        points += clientRightAnswerDragDropCounts[answerCount.Key] / (double)rightDragDropAnswerCounts[answerCount.Key];
                    }
                }



                var percent = Math.Round(points / (rightAnswerCounts.Count + rightDragDropAnswerCounts.Count) * 100);

                TempData["Tulemus"] = string.Format("Sinu tulemus on {0} %!", percent);

                TempData["Salvestatud"] = "Sinu andmed on salvestatud";
                return RedirectToAction("Index", "Home");
            }
            return RedirectToAction("Index", "Clients");


        }
    
    }
}
